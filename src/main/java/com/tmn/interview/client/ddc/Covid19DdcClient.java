package com.tmn.interview.client.ddc;

import com.tmn.interview.client.ddc.model.Covid19DdcModel;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;


@Log4j2
@Setter
@Getter
@Configuration
@ConfigurationProperties(prefix = "webclient.ddc-service")
public class Covid19DdcClient {

    private String host;
    private int maxConnections = 15;
    private Duration responseTimeout = Duration.ofSeconds(30);
    private Duration connectionTimeout = Duration.ofSeconds(10);

    public Flux<Covid19DdcModel.GetDataTodayCasesByProvinces.Response> getDataTodayCasesByProvinces() {
        //TODO - add logic to call : https://covid19.ddc.moph.go.th/api/Cases/today-cases-by-provinces

        return Flux.just(Covid19DdcModel.GetDataTodayCasesByProvinces.Response.builder().build());
    }

    public Mono<Covid19DdcModel.GetTodayCasesAll.Response> getTodayCasesAll() {
        //TODO - add logic to call : https://covid19.ddc.moph.go.th/api/Cases/today-cases-all

        return Mono.just(Covid19DdcModel.GetTodayCasesAll.Response.builder().build());
    }

}
