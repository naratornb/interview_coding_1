package com.tmn.interview.module.province.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.UtilityClass;

@UtilityClass
public class CovidProvinceModel {

    @UtilityClass
    public static class GetCovidStatByProvince {
        @Setter
        @Getter
        @JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
        @Builder
        public static class Response {
            private String province;
            private Double percentage;
        }

    }
}
