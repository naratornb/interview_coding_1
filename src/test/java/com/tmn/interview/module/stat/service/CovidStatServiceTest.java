package com.tmn.interview.module.stat.service;

import com.tmn.interview.client.ddc.Covid19DdcClient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith({MockitoExtension.class})
public class CovidStatServiceTest {


    @Mock
    private Covid19DdcClient covid19DdcClient;

    private CovidStatService covidStatService;
    @BeforeEach
    void beforeEach(){
        covidStatService = new CovidStatService(covid19DdcClient);
    }

    @Test
    void getCovidStatMaximumProvince_Success() {
        //TODO - Create unit test for call api success

    }

    @Test
    void getCovidStatMaximumProvince_Fail_Connection_Error() {
        //TODO - Create unit test for call api fail connection error

    }

    @Test
    void getCovidStatMaximumProvince_Fail_Connection_TimeOut() {
        //TODO - Create unit test for call api fail connection timeout

    }

    @Test
    void getCovidStatMaximumProvince_Fail_System_Error() {
        //TODO - Create unit test for call api fail system error

    }
}
