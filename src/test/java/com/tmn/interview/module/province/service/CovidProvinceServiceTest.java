package com.tmn.interview.module.province.service;

import com.tmn.interview.client.ddc.Covid19DdcClient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith({MockitoExtension.class})
class CovidProvinceServiceTest {

    @Mock
    private Covid19DdcClient covid19DdcClient;

    private CovidProvinceService covidProvinceService;

    @BeforeEach
    void beforeEach(){
        covidProvinceService = new CovidProvinceService(covid19DdcClient);
    }


    @Test
    void getCovidStatByProvince_Success() {
        //TODO - Create unit test for call api success

    }

    @Test
    void getCovidStatByProvince_Fail_Connection_Error() {
        //TODO - Create unit test for call api fail connection error
    }

    @Test
    void getCovidStatByProvince_Fail_Connection_TimeOut() {
        //TODO - Create unit test for call api fail connection timeout
    }

    @Test
    void getCovidStatByProvince_Fail_System_Error() {
        //TODO - Create unit test for call api fail system error
    }
}
