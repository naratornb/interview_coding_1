package com.tmn.interview.module.province.controller;

import com.tmn.interview.exception.ExceptionHandling;
import com.tmn.interview.module.province.service.CovidProvinceService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@ExtendWith({MockitoExtension.class})
class CovidProvinceControllerTests {

    private MockMvc mockMvc;

    @Mock
    private CovidProvinceService covidProvinceService;

    @BeforeEach
    void beforeEach() {
        mockMvc = standaloneSetup(new CovidProvinceController(covidProvinceService))
                .setControllerAdvice(new ExceptionHandling())
                .build();
    }

    @Test
    void getCovidStatByProvince_Success() {
    //TODO - Create unit test for call api success

    }

    @Test
    void getCovidStatByProvince_Fail_Connection_Error() {
        //TODO - Create unit test for call api fail connection error
    }

    @Test
    void getCovidStatByProvince_Fail_Connection_TimeOut() {
        //TODO - Create unit test for call api fail connection timeout
    }

    @Test
    void getCovidStatByProvince_Fail_System_Error() {
        //TODO - Create unit test for call api fail system error
    }

}
